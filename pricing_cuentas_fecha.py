import pandas as pd
import numpy as np
import psycopg2 as db
import logging
import sys
import datetime
import queries as qp
try:
    conn = db.connect(dbname='pol_v4', user='readonly', host='172.18.35.22', password='YdbLByGopWPS4zYi8PIR')
    cursor = conn.cursor()
except:
    logging.error('Cannot connect to database. Please run this script again')
    sys.exit()

start = datetime.date(2017, 12, 01)
end = datetime.date(2017, 12, 31)
days = end - start
pricingchanges = pd.DataFrame()

for x in xrange(0,days.days+1):
    date = start + datetime.timedelta(days=x)
    accounts = qp.listacuentas(date)
    if len(accounts)!=0:
        cambios = qp.controlcambioscuentas(accounts,date)
        pricingchanges = pricingchanges.append(cambios, ignore_index=True)
        cambios = cambios.iloc[0:0]

filename = 'Pricing_changes_Nov-2017_Jan-2018.xlsx'
writer = pd.ExcelWriter(filename)
pricingchanges.to_excel(writer)
writer.save()
