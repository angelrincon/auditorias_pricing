#!/usr/bin/env python
import pandas as pd
import psycopg2 as db
import logging
import sys
import datetime
import numpy as np

# Configurar Logger
logging.basicConfig(filename='queries.log', filemode='w', level=logging.DEBUG)
# Intentar Conexion a BD
try:
    conn = db.connect(dbname='pol_v4', user='readonly', host='172.18.35.22', password='YdbLByGopWPS4zYi8PIR')
    cursor = conn.cursor()
except:
    logging.error('Cannot connect to database. Please run this script again')
    sys.exit()

def listacuentas(today):
    tomorrow = today + datetime.timedelta(days=1)
    logging.warning('Searching account id with changes on %(today)s', {'today':str(today)})
    cursor.execute("""SELECT c.cuenta_id FROM audit.cuenta_aud c
    inner join audit.revision_auditoria ra ON c.rev = ra.revision_id 
    INNER JOIN pps.cuenta pc on (c.cuenta_id=pc.cuenta_id)
    WHERE (to_timestamp(fecha_revision/1000))>=%(today)s and (to_timestamp(fecha_revision/1000))<%(tomorrow)s
    and pc.fecha_creacion<%(today)s order by c.cuenta_id""",{'today':today,'tomorrow':tomorrow})
    cuentas = pd.DataFrame(cursor.fetchall())
    if len(cuentas)!=0:
        cuentas.columns = ['cuenta_id']
        cuentas = cuentas.drop_duplicates()
    logging.warning('Search finished: %(cantidad)s have received changes', {'cantidad':len(cuentas)})
    return cuentas

# ----------------------------------------------------------------------------------------------------------------------
# CONTROL DE CAMBIOS CUENTAS:
# CORRE POR EL LISTADO DE CUENTAS TRAYENDO LA CONSULTA DE LOS CAMBIOS A LA FECHA Y ANEXANDO EL ULTIMO CAMBIO
# ANTERIOR A LA FECHA DE EJECUCION DEL REPORTE.
# RETORNA UN DATAFRAME CON EL LISTADO DE TODOS LOS CAMBIOS PARA EL LISTADO DE CUENTAS.
def controlcambioscuentas(cuentas,today):

    cuentas = cuentas['cuenta_id'].tolist()
    temp = []
    tomorrow=today + datetime.timedelta(days=1)
    logging.warning('Starting check for %(today)s', {'today':str(today)})
    audcuentas = pd.DataFrame()
    for index in range(0, len(cuentas)):
        cid = cuentas[index]   # Indexar cuentas Id
        logging.info('Checking account %(cid)s', {'cid':cid})
        # Ejecutar consulta de fecha actual
        cursor.execute("""SELECT to_timestamp(fecha_revision/1000) as "fec_cambio", c.rev,  uw.email, c.cuenta_id, 
        c.nombre, c.perfil_usuario_id, c.grupo_perfil_cobranza_id
        FROM audit.cuenta_aud c
        inner join audit.revision_auditoria ra ON (c.rev = ra.revision_id)
        inner join pps.usuario_web uw on (c.usuario_modificacion_id=uw.usuario_web_id)
        WHERE (to_timestamp(fecha_revision/1000))>=%(today)s and (to_timestamp(fecha_revision/1000))<%(tomorrow)s
        and c.cuenta_id=%(cid)s""", {'cid': cid,'today':today,'tomorrow':tomorrow})
        for i in xrange(cursor.rowcount):
            temp.append(cursor.fetchone())
        # Ejecutar consulta de cambio inmediatamente anterior
        cursor.execute("""SELECT to_timestamp(fecha_revision/1000) as "fec_cambio", c.rev,  uw.email, c.cuenta_id, 
        c.nombre, c.perfil_usuario_id, c.grupo_perfil_cobranza_id
        FROM audit.cuenta_aud c
        inner join audit.revision_auditoria ra ON (c.rev = ra.revision_id)
        inner join pps.usuario_web uw on (c.usuario_modificacion_id=uw.usuario_web_id)
        WHERE (to_timestamp(fecha_revision/1000))<%(today)s and c.cuenta_id=%(cid)s 
        order by "fec_cambio" desc limit 1""", {'cid': cid, 'today':today})
        i = 0
        for i in xrange(cursor.rowcount):
            temp.append(cursor.fetchone())
        cambios = pd.DataFrame(temp,columns = ['fecha_rev', 'rev_id', 'email', 'cuenta_id', 'nombre_cuenta',
                                               'perfil_usuario_id','grupo_perfil_cobranza'])
        cambios['fecha_rev'] = pd.to_datetime(cambios.fecha_rev)
        cambios = cambios.sort_values(by='fecha_rev')
        cambiosord2 = cambios.shift()
        cambiosord2.columns = ['fecha_revprev', 'rev_idprev', 'emailprev', 'cuenta_idprev',
                               'nombre_cuenta_prev', 'perfil_usuario_idprev','grupo_perfil_cobranzaprev']
        df = pd.concat([cambios, cambiosord2], axis=1)
        # En caso de que sea un primer cambio, ejecutar script de identificacion
        if len(df)!=0:
            df.drop(df.index[0], inplace=True)
            df['cambio'] = np.where(df['perfil_usuario_id'] != df['perfil_usuario_idprev'], 'perfilcobranza', None)
            df = df[df.cambio.notnull()]
            audcuentas = audcuentas.append(df, ignore_index=True)
        logging.info('Found %(numbers)s changes',{'numbers':len(df)})

        # Limpiar variables iterativas
        df = df.iloc[0:0]
        cambios=cambios.iloc[0:0]
        cambiosord2=cambiosord2.iloc[0:0]
        temp=[]
        logging.info('Account Id %(cid)s finished',{'cid':cid})
    # Eliminar Columnas innecesarias
    logging.warning('Process Finished. Proceeding to concatenate and save')
    return audcuentas

#-----------------------------------------------------------------------------------------------------------------------

def listaperfiles(today):
    tomorrow = today + datetime.timedelta(days=1)
    logging.warning('Searching account id with changes on %(today)s', {'today':str(today)})
    cursor.execute("""SELECT pc.perfil_cobranza_id FROM audit.perfil_cobranza_aud pc
    inner join audit.revision_auditoria ra ON pc.rev = ra.revision_id 
    WHERE (to_timestamp(fecha_revision/1000))>=%(today)s and (to_timestamp(fecha_revision/1000))<%(tomorrow)s
    order by pc.perfil_cobranza_id""",{'today':today,'tomorrow':tomorrow})
    perfiles = pd.DataFrame(cursor.fetchall())
    if len(perfiles)!=0:
        perfiles.columns = ['perfil_cobranza_id']
        perfiles = perfiles.drop_duplicates()
    logging.warning('Search finished: %(cantidad)s have received changes', {'cantidad':len(perfiles)})
    return perfiles


#-----------------------------------------------------------------------------------------------------------------------

def controlcambiospricing(perfiles,today):

    perfiles = perfiles['perfil_cobranza_id'].tolist()
    temp = []
    tomorrow=today + datetime.timedelta(days=1)
    logging.warning('Starting check for %(today)s', {'today':str(today)})
    audperfiles = pd.DataFrame()
    for index in range(0, len(perfiles)):
        pcid = perfiles[index]   # Indexar cuentas Id
        logging.info('Checking profile %(pcid)s', {'pcid':pcid})
        # Ejecutar consulta de fecha actual
        cursor.execute("""SELECT to_timestamp(fecha_revision/1000) as "fec_cambio", pc.rev,  uw.email, 
        pc.perfil_cobranza_id ,pu.nombre, pu.descripcion, pu.tipo, pu.predeterminado,  
        pc.tipo_medio_pago, pc.modelo_acreditacion, pc.dias_modelo_acreditacion, cpa.moneda_iso_4217, 
        cpa.comision_plana_comercio, cpa.comision_porcentual_comercio, cpa.comision_maxima_comercio, 
        cpa.comision_plana_pagador, cpa.comision_minima_pagador, cpa.comision_maxima_pagador

        FROM audit.perfil_cobranza_aud pc 

        inner join pps.usuario_web uw on (pc.usuario_modificacion_id=uw.usuario_web_id) 
        inner join audit.revision_auditoria ra ON (pc.rev = ra.revision_id)
        inner join audit.comision_pricing_aud cpa on (pc.perfil_cobranza_id=cpa.perfil_cobranza_id)
        left join pps.grupo_perfil_cobranza gpc on (pc.grupo_perfil_cobranza_id=gpc.grupo_perfil_cobranza_id)
        left join pps.perfil_usuario pu on (gpc.usuario_perfil_id=pu.perfil_usuario_id)
        
        WHERE (to_timestamp(fecha_revision/1000))>=%(today)s and (to_timestamp(fecha_revision/1000))<%(tomorrow)s
        and pc.perfil_cobranza_id=%(pcid)s""", {'pcid': pcid,'today':today,'tomorrow':tomorrow})
        for i in xrange(cursor.rowcount):
            temp.append(cursor.fetchone())
        # Ejecutar consulta de cambio inmediatamente anterior
        cursor.execute("""SELECT to_timestamp(fecha_revision/1000) as "fec_cambio", pc.rev,  uw.email, 
        pc.perfil_cobranza_id ,pu.nombre, pu.descripcion, pu.tipo, pu.predeterminado,  
        pc.tipo_medio_pago, pc.modelo_acreditacion, pc.dias_modelo_acreditacion, cpa.moneda_iso_4217, 
        cpa.comision_plana_comercio, cpa.comision_porcentual_comercio, cpa.comision_maxima_comercio, 
        cpa.comision_plana_pagador, cpa.comision_minima_pagador, cpa.comision_maxima_pagador

        FROM audit.perfil_cobranza_aud pc 

        inner join pps.usuario_web uw on (pc.usuario_modificacion_id=uw.usuario_web_id) 
        inner join audit.revision_auditoria ra ON (pc.rev = ra.revision_id)
        inner join audit.comision_pricing_aud cpa on (pc.perfil_cobranza_id=cpa.perfil_cobranza_id)
        left join pps.grupo_perfil_cobranza gpc on (pc.grupo_perfil_cobranza_id=gpc.grupo_perfil_cobranza_id)
        left join pps.perfil_usuario pu on (gpc.usuario_perfil_id=pu.perfil_usuario_id)
        
        WHERE (to_timestamp(fecha_revision/1000))<%(today)s and pc.perfil_cobranza_id=%(pcid)s 
        order by "fec_cambio" desc limit 1""", {'pcid': pcid, 'today':today})
        i = 0
        for i in xrange(cursor.rowcount):
            temp.append(cursor.fetchone())
        cambios = pd.DataFrame(temp,columns = ['fecha_rev', 'rev_id', 'email', 'perfil_cobranza_id', 'nombre_perfil',
                                               'descripcion','tipo','predeterminado','tipo_medio_pago','modelo_acreditacion',
                                               'dias_modelo','moneda','comision_plana_comercio','comision_porc_comercio',
                                               'comision_maxima_comercio','comision_plana_pagador','comision_minima_pagador',
                                               'comision_maxima_pagador'])
        cambios['fecha_rev'] = pd.to_datetime(cambios.fecha_rev)
        cambios = cambios.sort_values(by='fecha_rev')
        cambiosord2 = cambios.shift()
        cambiosord2.columns = ['fecha_rev_prev', 'rev_id_prev', 'email_prev', 'perfil_cobranza_id_prev', 'nombre_perfil_prev',
                                'descripcion_prev','tipo_prev','predeterminado_prev','tipo_medio_pago_prev','modelo_acreditacion_prev',
                                'dias_modelo_prev','moneda_prev','comision_plana_comercio_prev','comision_porc_comercio_prev',
                                'comision_maxima_comercio_prev','comision_plana_pagador_prev','comision_minima_pagador_prev',
                                'comision_maxima_pagador_prev']
        df = pd.concat([cambios, cambiosord2], axis=1)
        df['cambio']=np.NaN
        # En caso de que sea un primer cambio, ejecutar script de identificacion
        if len(df)!=0:
            df.drop(df.index[0], inplace=True)
            df['cambio'] = np.where(df['tipo_medio_pago']==df['tipo_medio_pago_prev'],
                                    (np.where( df['comision_plana_comercio'] != df['comision_plana_comercio_prev'],
                                    'comision_plana_comercio',df['cambio'])),df['cambio'])
            df['cambio'] = np.where(df['tipo_medio_pago'] == df['tipo_medio_pago_prev'],
                                    (np.where(df['comision_porc_comercio'] != df['comision_porc_comercio_prev'],
                                    'comision_porc_comercio',df['cambio'])),df['cambio'])
            df['cambio'] = np.where(df['tipo_medio_pago'] == df['tipo_medio_pago_prev'],
                                    (np.where(df['comision_maxima_comercio'] != df['comision_maxima_comercio_prev'],
                                    'comision_maxima_comercio',df['cambio'])),df['cambio'])
            df['cambio'] = np.where(df['tipo_medio_pago'] == df['tipo_medio_pago_prev'],
                                    (np.where(df['comision_plana_pagador'] != df['comision_plana_pagador_prev'],
                                    'comision_plana_pagador',df['cambio'])),df['cambio'])
            df['cambio'] = np.where(df['tipo_medio_pago'] == df['tipo_medio_pago_prev'],
                                    (np.where(df['comision_minima_pagador'] != df['comision_minima_pagador_prev'],
                                    'comision_minima_pagador_',df['cambio'])),df['cambio'])
            df['cambio'] = np.where(df['tipo_medio_pago'] == df['tipo_medio_pago_prev'],
                                    (np.where(df['comision_maxima_pagador'] != df['comision_maxima_pagador_prev'],
                                    'comision_maxima_pagador',df['cambio'])),df['cambio'])
            df = df[df.cambio.notnull()]
            audperfiles = audperfiles.append(df, ignore_index=True)
        logging.info('Found %(numbers)s changes',{'numbers':len(df)})

        # Limpiar variables iterativas
        df = df.iloc[0:0]
        cambios=cambios.iloc[0:0]
        cambiosord2=cambiosord2.iloc[0:0]
        temp=[]
        logging.info('Pricing profile Id %(pcid)s finished',{'pcid':pcid})
    # Eliminar Columnas innecesarias
    logging.warning('Process Finished. Proceeding to concatenate and save')
    return audperfiles

